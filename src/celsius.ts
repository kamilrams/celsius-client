import { Chart } from 'chart.js';
import moment from 'moment';
import 'moment/locale/pl';

// types
interface Measure {
    id: number;
    temperature: number;
    date: string;
}

type Measures = Measure[];

// config
const apiUrl = 'http://celsius-api.kamilrams.pl/';
moment.locale('pl');

// elements
const canvas = <HTMLCanvasElement> document.getElementById('celsius');
const highestTemp = <HTMLElement> document.getElementById('highest-temp');
const lowestTemp = <HTMLElement> document.getElementById('lowest-temp');
const averageTemp = <HTMLElement> document.getElementById('average-temp');
const totalMeasures = <HTMLElement> document.getElementById('total-measures');

// draw chart
const drawChart = (measures: Measures): void => {
    const dataSet = {
        labels: mapMeasureDates(measures),
        datasets: [
            {
                label: 'Temperatura',
                data: measures.map(measure => measure.temperature),
                borderColor: '#e17055',
                backgroundColor: '#fab1a0',
            }
        ],
    }

    new Chart(canvas, {
        type: 'line',
        data: dataSet,
    });
};

// info about measures
const fullfillInfo = (measures: Measures): void => {
    const highest = measures.sort((a, b) => b.temperature - a.temperature)[0];
    const lowest = measures.sort((a, b) => a.temperature - b.temperature)[0];
    const total = measures.length;
    const average = measures.reduce((sum, measure) => sum + measure.temperature, 0) / total;

    highestTemp.innerText = highest.temperature.toString();
    lowestTemp.innerText = lowest.temperature.toString();
    averageTemp.innerText = average.toFixed(2).toString();
    totalMeasures.innerText = total.toString();
}

const mapMeasureDates = (measures: Measures): string[] => {
    return measures.map(measure => {
        const date = new Date(measure.date);

        return moment(date).format('LLL');
    });
}

// load data from api
async function loadData() {
    const response = await fetch(apiUrl);
    const measures = <Measures> await response.json();

    drawChart(measures);
    fullfillInfo(measures);
}

loadData();